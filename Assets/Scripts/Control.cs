﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    [SerializeField] GameObject[] figures;
    private GameObject inst_Figure;
    private GameObject next_Figure; 
    float corY = 5;    
    
    // Start is called before the first frame update
    void Start()
    {
        
        inst_Figure = Instantiate(figures[Random.Range(0, figures.Length)], new Vector3(0, corY, 0), Quaternion.identity);   
        next_Figure = Instantiate(figures[Random.Range(0, figures.Length)], new Vector3(0, 6, -6), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        corY -= 2f*Time.deltaTime;        
        inst_Figure.transform.position = new Vector3(0, corY, 0);    
        
        if(inst_Figure.transform.position.y <= -10.5f)
        {            
            Destroy(inst_Figure);            
            corY = 5;
            inst_Figure = next_Figure;
            next_Figure = next_Figure = Instantiate(figures[Random.Range(0, figures.Length)], new Vector3(0, 6, -6), Quaternion.identity);
        }        
    }
}
